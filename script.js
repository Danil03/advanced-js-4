fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => {
    if (!response.ok) {
      throw new Error('Помилка при завантаженні даних: ' + response.statusText);
    }
    return response.json();
  })
  .then(films => {
    displayFilms(films);
    return Promise.all(films.map(getCharactersForFilm));
  })
  .catch(error => {
    console.error(error);
  });

function displayFilms(films) {
  const filmListContainer = document.getElementById('film-list');
  films.forEach(function(film) {
    const filmElement = document.createElement('div');
    filmElement.classList.add('film');
    filmElement.innerHTML = '<h3>Episode ' + film.episodeId + ': ' + film.title + '</h3>' +
                            '<p>' + film.openingCrawl + '</p>';
    filmElement.setAttribute('id', 'film-' + film.episodeId); 
    filmListContainer.appendChild(filmElement);
  });
}

function getCharactersForFilm(film) {
  const characterPromises = film.characters.map(characterURL => {
    return fetch(characterURL)
      .then(response => {
        if (!response.ok) {
          throw new Error('Помилка при отриманні персонажів: ' + response.statusText);
        }
        return response.json();
      })
      .then(characterData => {
        const filmElement = document.getElementById('film-' + film.episodeId);
        const characterList = filmElement.querySelector('.character-list') || document.createElement('ul');
        const characterItem = document.createElement('li');
        characterItem.textContent = characterData.name;
        characterList.appendChild(characterItem);
        filmElement.appendChild(characterList);
      })
      .catch(error => {
        console.error('Помилка при отриманні персонажів:', error);
      });
  });
  return Promise.all(characterPromises);
}


// AJAX це технологія за допомогою якої сторінка може взаємодіяти з сервером без перезавантаження сторінки, це робиться для того
//  щоб веб-сторінка могла відправляти запити на сервер та отримувати дані.